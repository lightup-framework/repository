<?php

namespace Lightup\Repository;

use Lightup\Framework\Configuration;
use Lightup\Framework\Container;
use Lightup\Framework\ServiceProvider;
use Lightup\Repository\Exception\NoConfigurationProvidedException;
use Lightup\Repository\Exception\NoParamsProvidedException;
use Lightup\Repository\Exception\NoTableNameProvided;
use Lightup\Repository\Exception\NotSupportedMethodType;
use Lightup\Support\File\Files;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionNamedType;

class RepositoryServiceProvider implements ServiceProvider
{
    private Configuration $configuration;
    private Container $container;
    private Builder $builder;
    private Parser $parser;

    public function __construct(Configuration $configuration, Container $container, Builder $builder, Parser $parser)
    {
        $this->configuration = $configuration;
        $this->container = $container;
        $this->builder = $builder;
        $this->parser = $parser;
    }

    /**
     * @throws NotSupportedMethodType
     * @throws ReflectionException
     * @throws NoConfigurationProvidedException
     * @throws NoTableNameProvided
     * @throws NoParamsProvidedException
     */
    public function boot(): void
    {
        $interfacesDirectory = $this->configuration->get('repository.interfaces');
        $compileDirectory = $this->configuration->get('repository.compilation');
        $env = $this->configuration->get('app.env', 'develop');

        if ($interfacesDirectory == null || $compileDirectory == null) {
            throw new NoConfigurationProvidedException();
        }

        if ($env === 'develop') {
            Files::removeDirectory($compileDirectory, true);
        }

        if (Files::isDirectoryEmpty($compileDirectory)) {
            $filesInInterfacesDirectory = Files::getFiles($interfacesDirectory);

            foreach ($filesInInterfacesDirectory as $file) {
                $fullClassName = $this->parser->parseFullClassName($file);
                $className = $this->parser->parseSimpleClassName($fullClassName);
                $implementationClass = $className . 'Compiled';
                $implementationFullClassName = 'Compiled\\Repository\\' . $implementationClass;

                $reflectionClass = new ReflectionClass($fullClassName);
                $tableName = $this->parser->parseTableName($reflectionClass);
                $modelClass = $this->parser->parseModelClass($reflectionClass);
                $modelClass = ltrim($modelClass, '\\');
                $modelClass = '\\' . $modelClass;

                $classBody = '';

                foreach ($reflectionClass->getMethods() as $reflectionMethod) {
                    $classBody .= $this->getFunction($reflectionMethod, $tableName, $modelClass);
                    $classBody .= "\n\n";
                }

                $usage = 'use ' . $fullClassName . ';';
                $compiledClass = Replacer::replaceClass('Compiled\\Repository', $usage, $implementationClass, $className, $classBody);
                $implementationPath = $compileDirectory . DIRECTORY_SEPARATOR . $implementationClass . '.php';

                Files::putFileContent($implementationPath, $compiledClass);
                require_once $implementationPath;
                $this->container->bind($fullClassName, $implementationFullClassName);
            }
        }
    }

    public function shutdown(): void
    {

    }

    /**
     * @throws NotSupportedMethodType
     * @throws NoParamsProvidedException
     */
    private function getFunction(ReflectionMethod $reflectionMethod, string $tableName, string $modelClass): string
    {
        $methodName = $reflectionMethod->getName();
        $methodArguments = $reflectionMethod->getParameters();

        $returnType = '';
        if ($reflectionMethod->hasReturnType()) {
            $reflectionType = $reflectionMethod->getReturnType();
            if (get_class($reflectionType) === substr(ReflectionNamedType::class, strrpos(ReflectionNamedType::class, '\\'))) {
                $returnType = $reflectionType->getName();
                $returnType = (str_contains($returnType, '\\') ? '\\' : '') . $returnType;
                if ($returnType !== 'null') {
                    $returnType = ($reflectionType->allowsNull() ? '?' : '') . $returnType;
                }
            } else {
                foreach ($reflectionType->getTypes() as $type) {
                    $partialReturnType = $type->getName();
                    $partialReturnType = (str_contains($partialReturnType, '\\') ? '\\' : '') . $partialReturnType;
                    if ($partialReturnType !== 'null') {
                        $partialReturnType = ($type->allowsNull() ? '?' : '') . $partialReturnType;
                    }
                    $returnType .= $partialReturnType . '|';
                }
                $returnType = substr($returnType, 0, -1);
            }
        }

        $result = '';
        if (str_starts_with($methodName, 'findBy')) {
            $builder = $this->builder->getSelectBuilder($methodName, $tableName, $methodArguments);
            $result = Replacer::replaceFindBy($methodName, $reflectionMethod->getParameters(), $builder, $modelClass, $returnType);
        } else if (str_starts_with($methodName, 'existsBy')) {
            $builder = $this->builder->getSelectBuilder($methodName, $tableName, $methodArguments);
            $result = Replacer::replaceExistsBy($methodName, $reflectionMethod->getParameters(), $builder, $modelClass);
        } else if (str_starts_with($methodName, 'getBy')) {
            $builder = $this->builder->getSelectBuilder($methodName, $tableName, $methodArguments);
            $result = Replacer::replaceGetBy($methodName, $reflectionMethod->getParameters(), $builder, $modelClass, $returnType);
        } else if (str_starts_with($methodName, 'create')) {
            $result = Replacer::replaceCreate($modelClass, $tableName);
        } else if (str_starts_with($methodName, 'delete')) {
            $builder = $this->builder->getDeleteBuilder($methodName, $tableName, $methodArguments);
            $result = Replacer::replaceDeleteBy($methodName, $reflectionMethod->getParameters(), $builder, $tableName);
        } else if (str_starts_with($methodName, 'findAll')) {
            $builderFindAll = $this->builder->getBuilderFindAll($methodName, $tableName);
            $result = Replacer::replaceFindAll($methodName, $reflectionMethod->getParameters(), $builderFindAll, $modelClass);
        } else {
            throw new NotSupportedMethodType();
        }

        return $result;
    }
}