<?php

namespace Lightup\Repository;

use Lightup\Repository\Attributes\Model;
use Lightup\Repository\Attributes\Table;
use Lightup\Repository\Exception\NoTableNameProvided;
use Lightup\Support\File\Files;
use ReflectionClass;

class Parser
{
    private static array $SPLIT = [
        'findBy',
        'findAll',
        'getBy',
        'existsBy',
        'create',
        'deleteBy',
        'OrderBy',
        'Desc',
        'Asc',
        'And',
    ];

    private static array $KEY_WORDS = [
        'findBy',
        'findAll',
        'getBy',
        'existsBy',
        'create',
        'deleteBy',
        'OrderBy',
        'Desc',
        'Asc',
    ];

    private static array $KEEP_KEY_WORDS = [
        'Asc',
        'Desc',
    ];

    public function parseParams(array &$parsed): array
    {
        array_shift($parsed);
        $parsedCopy = $parsed;
        $params = [];
        $orderBy = [];

        foreach ($parsedCopy as $partial) {

            if (in_array($partial, self::$KEY_WORDS)) {
                break;
            }

            if (strcasecmp($partial, 'and') === 0) {
                array_shift($parsed);
                continue;
            }

            $params[] = strtolower($partial);
            array_shift($parsed);
        }

        if (!empty($parsed) && $parsed[0] === 'OrderBy') {
            $orderBy = $this->parseOrderBy($parsed);
        }

        return [
            'params' => $params,
            'orderBy' => $orderBy,
        ];
    }

    public function parseOrderBy(array &$parsed): array
    {
        array_shift($parsed);
        $parsedCopy = $parsed;
        $result = [];

        foreach ($parsedCopy as $partial) {
            if ($partial === '') {
                continue;
            }

            if (in_array($partial, self::$KEEP_KEY_WORDS) && !in_array($partial, self::$KEEP_KEY_WORDS)) {
                continue;
            }

            if (strcasecmp($partial, 'and') === 0) {
                array_shift($parsed);
                continue;
            }

            $result[] = strtolower($partial);
            array_shift($parsed);
        }

        return $result;
    }

    public function parseFullClassName(string $filename): string
    {
        $content = Files::getFileContent($filename);

        $namespace = [];
        preg_match("/namespace\s(.+?);/s", $content, $namespace);
        $namespace = $namespace[1];

        $className = explode(DIRECTORY_SEPARATOR, $filename);
        $className = $className[count($className) - 1];
        $className = explode(".", $className);
        $className = $className[0];

        return $namespace . '\\' . $className;
    }

    public function parseMethodName(string $methodName): array
    {
        $stringBuilder = '/(';
        foreach (self::$SPLIT as $keyWord) {
            $stringBuilder .= $keyWord . '|';
        }
        $stringBuilder = rtrim($stringBuilder, '|');
        $stringBuilder .= ')/';
        $partials = preg_split($stringBuilder, $methodName, -1, PREG_SPLIT_DELIM_CAPTURE);
        if (count($partials) > 1) {
            array_shift($partials);
        }

        return $partials;
    }

    /**
     * @throws NoTableNameProvided
     */
    public function parseTableName(ReflectionClass $reflectionClass): string
    {
        $attributes = $reflectionClass->getAttributes(Table::class);
        if (empty($attributes)) {
            throw new NoTableNameProvided();
        }

        $attribute = $attributes[0]->newInstance();
        return $attribute->getTableName();
    }

    /**
     * @throws NoTableNameProvided
     */
    public function parseModelClass(ReflectionClass $reflectionClass): string
    {
        $attributes = $reflectionClass->getAttributes(Model::class);
        if (empty($attributes)) {
            throw new NoTableNameProvided();
        }

        $attribute = $attributes[0]->newInstance();
        return $attribute->getClass();
    }

    public function parseSimpleClassName(string $fullClassName): string
    {
        $exploded = explode('\\', $fullClassName);
        return $exploded[count($exploded) - 1];
    }
}