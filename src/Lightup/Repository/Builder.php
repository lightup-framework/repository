<?php

namespace Lightup\Repository;

use Lightup\Repository\Exception\NoParamsProvidedException;
use Lightup\Repository\Exception\NotSupportedMethodType;

class Builder
{
    private static array $BUILDER_TYPES = [
        'findBy' => 'selectBuilder',
        'findAll' => 'selectBuilder',
        'getBy' => 'selectBuilder',
        'existsBy' => 'selectBuilder',
        'create' => 'insertBuilder',
        'deleteBy' => 'deleteBuilder'
    ];

    private Parser $parser;

    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * @throws NotSupportedMethodType
     */
    public function getBuilderType(string $methodType): string
    {
        if (!array_key_exists($methodType, self::$BUILDER_TYPES)) {
            throw new NotSupportedMethodType();
        }

        return self::$BUILDER_TYPES[$methodType];
    }

    /**
     * @throws NotSupportedMethodType
     * @throws NoParamsProvidedException
     */
    public function getSelectBuilder(string $methodName, string $tableName, array $methodArguments): string
    {
        $parsed = $this->parser->parseMethodName($methodName);
        $methodType = $parsed[0];
        [
            'params' => $params,
            'orderBy' => $orderBy,
        ] = $this->parser->parseParams($parsed);
        if (empty($params)) {
            throw new NoParamsProvidedException();
        }

        $builderType = $this->getBuilderType($methodType);

        $builder = 'Builder::';
        $builder .= $builderType . '()';
        if ($builderType === 'selectBuilder') {
            $builder .= '->select(\'*\')';
        }
        $builder .= '->table("' . $tableName . '")';

        foreach ($params as $param) {
            $builder .= '->where("' . $param . '", $' . $methodArguments[0]->getName() . ')';
            array_shift($methodArguments);
        }

        $builderOrderBy = $this->buildOrderBy($orderBy);
        if ($builderOrderBy !== '') {
            $builder .= $builderOrderBy;
        }

        $builder .= '->compile()';

        return $builder;
    }
    public function getDeleteBuilder(string $methodName, string $tableName, array $methodArguments): string
    {
        $parsed = $this->parser->parseMethodName($methodName);
        $methodType = $parsed[0];
        [
            'params' => $params,
            'orderBy' => $orderBy,
        ] = $this->parser->parseParams($parsed);
        if (empty($params)) {
            throw new NoParamsProvidedException();
        }

        $builderType = $this->getBuilderType($methodType);

        $builder = 'Builder::';
        $builder .= $builderType . '()';
        $builder .= '->table("' . $tableName . '")';

        foreach ($params as $param) {
            $builder .= '->where("' . $param . '", $' . $methodArguments[0]->getName() . ')';
            array_shift($methodArguments);
        }

        $builder .= '->compile()';

        return $builder;
    }

    /**
     * @throws NotSupportedMethodType
     */
    public function getBuilderFindAll(string $methodName, string $tableName): string
    {
        $parsed = $this->parser->parseMethodName($methodName);
        $methodType = $parsed[0];
        [
            'orderBy' => $orderBy
        ] = $this->parser->parseParams($parsed);
        $builderType = $this->getBuilderType($methodType);

        $builder = 'Builder::';
        $builder .= $builderType . '()';
        if ($builderType === 'selectBuilder') {
            $builder .= '->select(\'*\')';
        }
        $builder .= '->table("' . $tableName . '")';

        $builderOrderBy = $this->buildOrderBy($orderBy);
        if ($builderOrderBy !== '') {
            $builder .= $builderOrderBy;
        }
        $builder .= '->compile()';

        return $builder;
    }

    private function buildOrderBy(array $params): string
    {
        $builder = '';
        if (!empty($params)) {
            $lastElem = array_pop($params);
            if (!in_array($lastElem, ['asc', 'desc'])) {
                $params[] = $lastElem;
            }

            foreach ($params as $param) {
                $builder .= '->order("' . $param . '")';
            }

            if (in_array($lastElem, ['asc', 'desc'])) {
                $builder .= '->orderType("' . $lastElem . '")';
            }
        }

        return $builder;
    }
}