<?php

namespace Lightup\Repository;

use Lightup\Support\File\Files;

class Replacer
{
    private static string $STUB_DIRECTORY_PATH = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'stub' . DIRECTORY_SEPARATOR;

    public static function replaceClass(string $namespace, string $usage, string $class, string $interface, string $body): string
    {
        $stub = Files::getFileContent(self::$STUB_DIRECTORY_PATH . 'class.stub');
        $stub = self::replace_preg('namespace', $namespace, $stub);
        $stub = self::replace_preg('usage', $usage, $stub);
        $stub = self::replace_preg('class', $class, $stub);
        $stub = self::replace_preg('interface', $interface, $stub);
        return self::replace_preg('body', $body, $stub);
    }

    public static function replaceFindBy(string $methodName, array $arguments, string $builder, string $modelClass, string $returnType): string
    {
        return Replacer::replace('findBy.stub', $methodName, $arguments, $builder, $modelClass, $returnType);
    }

    public static function replaceFindAll(string $methodName, array $arguments, string $builder, string $modelClass): string
    {
        return Replacer::replace('findAll.stub', $methodName, $arguments, $builder, $modelClass, '');
    }

    public static function replaceDeleteBy(string $methodName, array $arguments, string $builder, string $tableName): string
    {
        return Replacer::replace('deleteBy.stub', $methodName, $arguments, $builder, '', '', $tableName);
    }

    public static function replaceExistsBy(string $methodName, array $arguments, string $builder, string $modelClass): string
    {
        return Replacer::replace('existsBy.stub', $methodName, $arguments, $builder, $modelClass, '');
    }

    public static function replaceGetBy(string $methodName, array $arguments, string $builder, string $modelClass, string $returnType): string
    {
        return Replacer::replace('getBy.stub', $methodName, $arguments, $builder, $modelClass, $returnType);
    }

    public static function replaceCreate(string $modelClass, string $tableName): string
    {
        return Replacer::replace('create.stub', '', [], '', $modelClass, '', $tableName);
    }

    private static function replace(string $filename, string $methodName, array $arguments, string $builder, string $modelClass, string $returnType, string $tableName = ''): string
    {
        $stub = Files::getFileContent(self::$STUB_DIRECTORY_PATH . $filename);
        $stub = self::replace_preg('name', $methodName, $stub);
        $stub = self::replace_preg('builder', $builder, $stub);
        $stub = self::replace_preg('params', self::argumentsToString($arguments), $stub);
        $stub = self::replace_preg('returnType', $returnType != '' ? ': ' . $returnType : '', $stub);
        $stub = self::replace_preg('tableName', $tableName, $stub);
        return self::replace_preg('modelClass', $modelClass, $stub);
    }

    private static function replace_preg(string $pattern, string $replacement, string $subject)
    {
        return preg_replace('/\{\{' . $pattern . '\}\}/', $replacement, $subject);
    }

    private static function argumentsToString(array $arguments) {
        $result = '';

        foreach ($arguments as $argument) {
            $result .= $argument->getType()->getName() . ' $' . $argument->getName() . ', ';
        }

        $result = rtrim($result, ', ');

        return $result;
    }
}